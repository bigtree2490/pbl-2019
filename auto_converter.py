# coding:utf-8
from openDataInfo import *
from initialization import *
from downloader import *
from export_csv import *
from convert_csv import *
from inserter import *

def auto_converter():
    # 初期化処理
    init_dir() # ディレクトリの初期化
    mk_database() # データベースの作成
    mk_tables() # テーブルの作成

    # メイン処理
    for i in range(len(openData)):
        # データの選択
        select = "d" + str(i + 1)
        # データの呼び出し
        useData = use_data(select)
        if useData == None:
            continue
        # 引数の決定
        filename = useData["filename"]
        extension = useData["extension"]
        url = useData["url"]
        table = useData["table"]
        # 実行
        downloader(filename, extension, url) # ファイルのダウンロード   
        export_csv(filename, extension) # ファイルをCSVにエキスポート
        convert_csv(filename) # 文字コードをutf-8に変換
        inserter(filename, table) # データを挿入


if __name__ == '__main__':
    auto_converter()