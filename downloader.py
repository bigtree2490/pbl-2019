# coding:utf-8
import os
import urllib.request

# ダウンロード処理
def downloader(filename, extension, url):
    print("[Download]")
    # ダウンロード先ディレクトリの指定
    download_dir = "./originFiles/"+extension
    # ダウンロード先ディレクトリの作成
    os.makedirs(download_dir, exist_ok=True)
    # ダウンロード先のパスを生成
    download_path = os.path.join(download_dir, filename+"."+extension)
    # ダウンロードを実行
    print(download_path+" is downloading.")
    urllib.request.urlretrieve(url, download_path)
    print('\033[32m'+"Downloaded!"+'\033[0m')


if __name__ == '__main__':
    print("Filename?：", end="")
    filename = input()
    print("Extension?：", end="")
    extension = input()
    print("URL?：", end="")
    url = input()
    downloader(filename, extension, url)

# 参考：https://pycarnival.com/urlsretreive_urlsopen/