# coding:utf-8
import glob
import csv
import os
from chardet.universaldetector import UniversalDetector
import subprocess

class Color:
    """
    標準出力に色を付けやすくするため

    使用例
    print(Color.RED+"fizzbuzz"+Color.END)
    結果
    fizzbuzz
    ※赤色になる
    """
    RED       = '\033[31m'
    GREEN     = '\033[32m'
    YELLOW    = '\033[33m'
    END       = '\033[0m'

def detect_character_code(pathname):
    """
    pathnameから指定されたファイルの文字コードを判別して
    ファイル名と文字コードのdictを返す

    :仮引数 pathname: 文字コードを判別したいファイルパス
    :返り値 files_code_dic: ファイル名がキー、文字コードが値のdict
    """
    files_code_dic = {}
    detector = UniversalDetector()
    for file in glob.glob(pathname):
        with open(file, 'rb') as f:
            detector.reset()
            for line in f.readlines():
                detector.feed(line)
                if detector.done:
                    break
            detector.close()
            files_code_dic[file] = detector.result['encoding']
    return files_code_dic

def convert_character_code(filepass, e_code):
    """
    filepassから指定されたファイルの文字コードを変換

    :第一引数 filepass: 文字コードを変換したいファイルパス
    :第二引数 e_code: 対象ファイルの予測文字コード
    :返り値: 最終メッセージ
    """
    #変換後の文字コード
    d_code="utf-8"
    if(e_code==d_code):
        print("対象ファイルの文字コードが"+d_code+"でした。")
        print("処理の必要はありません。")
        print("convert_character_code: status:"+Color.GREEN+"Successed!"+Color.END)
        return 0
    print(Color.GREEN+e_code+" >> "+d_code+" converter"+Color.END)
    print("対象ファイル（"+filepass+"）を"+e_code+"から"+d_code+"にコンバートします。")
    try:
        print(e_code+"として読み込み中")
        with open(filepass, "r", encoding=e_code, newline="") as f_in:
            reader = csv.reader(f_in, delimiter=',', quotechar='"')
            l = [row for row in reader]
            print(Color.GREEN+"Success!"+Color.END)
        f_in.close()
        print(d_code+"として書き出し中")
        with open(filepass, "w", encoding=d_code, newline="") as f_out:
            writer = csv.writer(f_out)
            writer.writerows(l)
            print(Color.GREEN+"Success!"+Color.END)
        f_out.close()
        print("convert_character_code: status:"+Color.GREEN+"Successed!"+Color.END)
        return 0
    # 例外処理
    except FileNotFoundError as e:
        print(Color.RED+"Failure"+Color.END)
        print(Color.RED+"FileNotFoundError：ファイルが見つかりませんでした。"+Color.END)
        print(e)
        print("convert_character_code: status:"+Color.RED+"Failed"+Color.END)
        return -1
    except csv.Error as e:
        print(Color.RED+"Failure"+Color.END)
        print(Color.RED+"csv.Error：エラー内容は以下です。"+Color.END)
        print(e)
        print("convert_character_code: status:"+Color.RED+"Failed"+Color.END)
        return -1
    except UnicodeDecodeError as e:
        print(Color.RED+"Failure"+Color.END)
        print(Color.YELLOW + "UnicodeDecodeError：python3は対象ファイルの文字コードを"+e_code+"ではないと判断しました。コンバートできませんでした。エラー内容は以下の通りです。" + Color.END)
        print(e)
        try:
            print(d_code+"として読み込みを試みます。")
            with open(filepass, "r", encoding=d_code, newline="") as f_in:
                reader = csv.reader(f_in, delimiter=',', quotechar='"')
                l = [row for row in reader]
                print(Color.GREEN+"Success!"+Color.END)
                print(Color.YELLOW+"対象ファイルは"+d_code+"の可能性が高いです。対象ファイルを"+d_code+"で読み取ると最初の３行は以下の通りです。"+Color.END)
                print(d_code,'\n',l[0],'\n',l[1],'\n',l[2],'\n')
            f_in.close()
            print("convert_character_code: status:"+Color.RED+"Failed"+Color.END)
            return -1
        except UnicodeDecodeError as e:
            print(Color.RED+"Failure"+Color.END)
            print(Color.YELLOW+"UnicodeDecodeError：対象ファイルは"+e_code+"でも"+d_code+"でもない文字コードである可能性があります。エラー内容は以下です。"+Color.END)
            print(e)
            print("convert_character_code: status:"+Color.RED+"Failed"+Color.END)
            return -1
    # https://note.nkmk.me/python-csv-reader-writer/

def convert_csv(filename):
    print("[Convert]")
    # 拡張子
    extension = 'csv'
    # コンバート元ディレクトリの指定
    target_path = './exportedFiles/'
    # コンバート元のパスを生成
    pathname = target_path + filename + '.' + extension
    # コンバート先ディレクトリの指定
    dest_dir = './convertedFiles'
    # コンバート先ディレクトリの作成
    os.makedirs(dest_dir, exist_ok=True)
    print(pathname+" is converting")
    # コンバート元の文字コードを推定
    files_character_code = detect_character_code(pathname)
    # コンバートの実行
    for filename in glob.glob(pathname):
        if(files_character_code[filename]==None):
            files_character_code[filename]='shift_jisx0213'
        res = convert_character_code(filename,files_character_code[filename])
        if res == 0:
            args = ['cp', filename, dest_dir]
            try:
                subprocess.run(args)
            except:
                print("Error.")
    print("Final status:"+Color.GREEN+"Successed!"+Color.END)
    print(Color.GREEN+"Converted!"+Color.END)
# 参考： https://kazusa-pg.com/python-detect-character-code/

if __name__ == '__main__':
    print("Filename?：", end="")
    filename = input()
    convert_csv(filename)

