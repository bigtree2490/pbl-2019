# coding:utf-8
# オープンデータ一覧
openData = {
    "d1": {
        "filename": "mergeFromCity",
        "extension": "csv",
        "url": "https://hinan.gsi.go.jp/hinanjocjp/hinanbasho/downloadAllCsv.php",
        "table": "shelter"
    },
    "d2": {
        "filename": "h30.1-6",
        "extension": "xlsx",
        "url": "https://ckan.open-governmentdata.org/dataset/dea9a604-a79e-4510-8b42-d3dbb52cab21/resource/04086207-e079-4e59-96c0-84fc141b5ce3/download/h30.1-6.xlsx",
        "table": "traffic_accident"
    },
    # "d3": {
    #     "filename": "h30.7-12",
    #     "extension": "xlsx",
    #     "url": "https://ckan.open-governmentdata.org/dataset/dea9a604-a79e-4510-8b42-d3dbb52cab21/resource/bfa84032-2088-4e22-ab07-45927adf64a6/download/h30.7-12.xlsx",
    #     "table": "traffic_accident"
    # },
    "d4": {
        "filename": "latlngfukuoka_2018buhinnerai",
        "extension": "csv",
        "url": "https://yuki-nissie-hoshi.com/fukuokaCrime2018/latlngfukuoka_2018buhinnerai.csv",
        "table": "buhinnerai"
    },
    "d5": {
        "filename": "latlngfukuoka_2018hittakuri",
        "extension": "csv",
        "url": "https://yuki-nissie-hoshi.com/fukuokaCrime2018/latlngfukuoka_2018hittakuri.csv",
        "table": "hittakuri"
    },
    "d6": {
        "filename": "latlngfukuoka_2018ootobaitou",
        "extension": "csv",
        "url": "https://yuki-nissie-hoshi.com/fukuokaCrime2018/latlngfukuoka_2018ootobaitou.csv",
        "table": "ootobaitou"
    },
    "d7": {
        "filename": "latlngfukuoka_2018syazyounerai",
        "extension": "csv",
        "url": "https://yuki-nissie-hoshi.com/fukuokaCrime2018/latlngfukuoka_2018syazyounerai.csv",
        "table": "syazyounerai"
    },
    "d8": {
        "filename": "latlngfukuoka_2018zidouhanbaikinerai",
        "extension": "csv",
        "url": "https://yuki-nissie-hoshi.com/fukuokaCrime2018/latlngfukuoka_2018zidouhanbaikinerai.csv",
        "table": "zidouhanbaikinerai"
    },
    "d9": {
        "filename": "latlngfukuoka_2018zidousyatou",
        "extension": "csv",
        "url": "https://yuki-nissie-hoshi.com/fukuokaCrime2018/latlngfukuoka_2018zidousyatou.csv",
        "table": "zidousyatou"
    },
    "d10": {
        "filename": "latlngfukuoka_2018zitensyatou",
        "extension": "csv",
        "url": "https://yuki-nissie-hoshi.com/fukuokaCrime2018/latlngfukuoka_2018zitensyatou.csv",
        "table": "zitensyatou"
    },
}

# テストデータ
testData = {
    "td1": {
        "filename": "publicHistoryListData",
        "extension": "csv",
        "url": "https://hinan.gsi.go.jp/hinanjocjp/defaultFtpData/publicHistoryCSV/publicHistoryListData.csv",
        "table" : None
    },
    "td2": {
        "filename": "data",
        "extension": "csv",
        "url":"http://192.168.33.10/converter/testData/sampleData/data.csv",
        "table" : None
    }
}

# データの選択
def use_data(select):
    try:
        useData = {}
        useData = openData[select]
        return useData
    except KeyError:
        return None