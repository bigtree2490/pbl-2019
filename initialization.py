# coding:utf-8
import subprocess
import MySQLdb
import time

def init_dir():
    args = ['rm', '-rf', './originFiles', './exportedFiles', './convertedFiles']
    try:
        subprocess.run(args)
    except:
        print("Error.")

def mk_database():
    connection = MySQLdb.connect(
        user="uuuuuuu",
        passwd="ppppppp",
        charset="utf8mb4"
    )
    cursor = connection.cursor()
    sql = "CREATE DATABASE IF NOT EXISTS ddddddd;"
    cursor.execute(sql)
    # クエリの実行
    connection.commit()
    cursor.close()
    connection.close()

def mk_tables():
    # データベースと接続
    connection = MySQLdb.connect(
        db="ddddddd",
        user="uuuuuuu",
        passwd="ppppppp",
        charset="utf8mb4"
    )
    # クエリの準備
    sql = {
        "CREATE TABLE IF NOT EXISTS `testmap`.`traffic_accident` ( `id` INT NOT NULL AUTO_INCREMENT , `NO` INT NOT NULL , `計上所属` VARCHAR(256) NOT NULL , `所属コード` INT NOT NULL , `事故内容` VARCHAR(10) NOT NULL , `発生年月日` DATE NOT NULL , `発生時` INT NOT NULL , `発生曜日` VARCHAR(3) NOT NULL , `発生場所番地` VARCHAR(256) NOT NULL , `市区町村コード` INT NOT NULL , `道路形状` VARCHAR(256) NOT NULL , `事故類型` VARCHAR(20) NOT NULL , `甲_種別` VARCHAR(256) NOT NULL , `甲_年齢` VARCHAR(256) NOT NULL , `甲_損傷程度` VARCHAR(256) NOT NULL , `乙_種別` VARCHAR(256) NOT NULL , `乙_年齢` VARCHAR(256) NOT NULL , `乙_損傷程度` VARCHAR(256) NOT NULL , `路線` VARCHAR(256) NOT NULL , `昼夜` VARCHAR(10) NOT NULL , `天候` VARCHAR(10) NOT NULL , `死者数` VARCHAR(11) NULL DEFAULT '', `負傷者数` VARCHAR(11) NULL DEFAULT '', `発生場所緯度` DOUBLE NOT NULL , `発生場所経度` DOUBLE NOT NULL , `発生年` YEAR NOT NULL , `発生月` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE utf8mb4_general_ci;",
        "CREATE TABLE IF NOT EXISTS `testmap`.`shelter` ( `id` INT NOT NULL AUTO_INCREMENT , `市町村コード` INT NOT NULL , `市町村名` VARCHAR(256) NOT NULL , `NO` VARCHAR(256) NOT NULL , `施設・場所名` VARCHAR(256) NOT NULL , `住所` VARCHAR(256) NOT NULL , `洪水` VARCHAR(11) NULL DEFAULT '' , `崖崩れ、土石流及び地滑り` VARCHAR(11) NULL DEFAULT '' , `高潮` VARCHAR(11) NULL DEFAULT '' , `地震` VARCHAR(11) NULL DEFAULT '' , `津波` VARCHAR(11) NULL DEFAULT '' , `大規模な災害` VARCHAR(11) NULL DEFAULT '' , `内水氾濫` VARCHAR(11) NULL DEFAULT '' , `火山現象` VARCHAR(11) NULL DEFAULT '' , `指定避難所との重複` VARCHAR(11) NULL DEFAULT '' , 緯度 DOUBLE NOT NULL , 経度 DOUBLE NOT NULL , `備考` VARCHAR(256) NULL DEFAULT '' , PRIMARY KEY (`id`)) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE utf8mb4_general_ci;",
        "CREATE TABLE IF NOT EXISTS `testmap`.`buhinnerai` ( `id` INT NOT NULL AUTO_INCREMENT , `罪名` VARCHAR(100) NOT NULL , `手口` VARCHAR(100) NOT NULL , `管轄警察署（発生地）` VARCHAR(100) NOT NULL , `管轄交番・駐在所（発生地）` VARCHAR(100) NOT NULL , `市区町村コード（発生地）` INT NOT NULL , `都道府県（発生地）` VARCHAR(100) NOT NULL , `市区町村（発生地）` VARCHAR(100) NOT NULL , `町丁目（発生地）` VARCHAR(100) NOT NULL , `発生年月日（始期）` VARCHAR(20) NOT NULL , `発生時（始期）` VARCHAR(20) NOT NULL , `発生場所の属性` VARCHAR(100) NOT NULL , `現金以外の被害品` VARCHAR(100) NOT NULL , `経度` DOUBLE NOT NULL , `緯度` DOUBLE NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE utf8mb4_general_ci;"
        "CREATE TABLE IF NOT EXISTS `testmap`.`hittakuri` ( `id` INT NOT NULL AUTO_INCREMENT , `罪名` VARCHAR(100) NOT NULL , `手口` VARCHAR(100) NOT NULL , `管轄警察署（発生地）` VARCHAR(100) NOT NULL , `管轄交番・駐在所（発生地）` VARCHAR(100) NOT NULL , `市区町村コード（発生地）` INT NOT NULL , `都道府県（発生地）` VARCHAR(100) NOT NULL , `市区町村（発生地）` VARCHAR(100) NOT NULL , `町丁目（発生地）` VARCHAR(100) NOT NULL , `発生年月日（始期）` VARCHAR(20) NOT NULL , `発生時（始期）` VARCHAR(20) NOT NULL , `発生場所の属性` VARCHAR(100) NOT NULL , `被害者の性別` VARCHAR(100) NOT NULL ,`被害者の年齢` VARCHAR(100) NOT NULL ,`現金被害の有無` VARCHAR(100) NOT NULL , `経度` DOUBLE NOT NULL , `緯度` DOUBLE NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE utf8mb4_general_ci;",
        "CREATE TABLE IF NOT EXISTS `testmap`.`ootobaitou` ( `id` INT NOT NULL AUTO_INCREMENT , `罪名` VARCHAR(100) NOT NULL , `手口` VARCHAR(100) NOT NULL , `管轄警察署（発生地）` VARCHAR(100) NOT NULL , `管轄交番・駐在所（発生地）` VARCHAR(100) NOT NULL , `市区町村コード（発生地）` INT NOT NULL , `都道府県（発生地）` VARCHAR(100) NOT NULL , `市区町村（発生地）` VARCHAR(100) NOT NULL , `町丁目（発生地）` VARCHAR(100) NOT NULL , `発生年月日（始期）` VARCHAR(20) NOT NULL , `発生時（始期）` VARCHAR(20) NOT NULL , `発生場所の属性` VARCHAR(100) NOT NULL , `施錠関係` VARCHAR(100) NOT NULL ,`盗難防止装置の有無` VARCHAR(100) NOT NULL ,`現金以外の被害品` VARCHAR(100) NOT NULL , `経度` DOUBLE NOT NULL , `緯度` DOUBLE NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE utf8mb4_general_ci;",
        "CREATE TABLE IF NOT EXISTS `testmap`.`syazyounerai` ( `id` INT NOT NULL AUTO_INCREMENT , `罪名` VARCHAR(100) NOT NULL , `手口` VARCHAR(100) NOT NULL , `管轄警察署（発生地）` VARCHAR(100) NOT NULL , `管轄交番・駐在所（発生地）` VARCHAR(100) NOT NULL , `市区町村コード（発生地）` INT NOT NULL , `都道府県（発生地）` VARCHAR(100) NOT NULL , `市区町村（発生地）` VARCHAR(100) NOT NULL , `町丁目（発生地）` VARCHAR(100) NOT NULL , `発生年月日（始期）` VARCHAR(20) NOT NULL , `発生時（始期）` VARCHAR(20) NOT NULL , `発生場所の属性` VARCHAR(100) NOT NULL , `施錠関係` VARCHAR(100) NOT NULL ,`現金被害の有無` VARCHAR(100) NOT NULL , `経度` DOUBLE NOT NULL , `緯度` DOUBLE NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE utf8mb4_general_ci;",
        "CREATE TABLE IF NOT EXISTS `testmap`.`zidouhanbaikinerai` ( `id` INT NOT NULL AUTO_INCREMENT , `罪名` VARCHAR(100) NOT NULL , `手口` VARCHAR(100) NOT NULL , `管轄警察署（発生地）` VARCHAR(100) NOT NULL , `管轄交番・駐在所（発生地）` VARCHAR(100) NOT NULL , `市区町村コード（発生地）` INT NOT NULL , `都道府県（発生地）` VARCHAR(100) NOT NULL , `市区町村（発生地）` VARCHAR(100) NOT NULL , `町丁目（発生地）` VARCHAR(100) NOT NULL , `発生年月日（始期）` VARCHAR(20) NOT NULL , `発生時（始期）` VARCHAR(20) NOT NULL , `発生場所の属性` VARCHAR(100) NOT NULL ,`現金被害の有無` VARCHAR(100) NOT NULL , `経度` DOUBLE NOT NULL , `緯度` DOUBLE NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE utf8mb4_general_ci;",
        "CREATE TABLE IF NOT EXISTS `testmap`.`zidousyatou` ( `id` INT NOT NULL AUTO_INCREMENT , `罪名` VARCHAR(100) NOT NULL , `手口` VARCHAR(100) NOT NULL , `管轄警察署（発生地）` VARCHAR(100) NOT NULL , `管轄交番・駐在所（発生地）` VARCHAR(100) NOT NULL , `市区町村コード（発生地）` INT NOT NULL , `都道府県（発生地）` VARCHAR(100) NOT NULL , `市区町村（発生地）` VARCHAR(100) NOT NULL , `町丁目（発生地）` VARCHAR(100) NOT NULL , `発生年月日（始期）` VARCHAR(20) NOT NULL , `発生時（始期）` VARCHAR(20) NOT NULL , `発生場所の属性` VARCHAR(100) NOT NULL , `施錠関係` VARCHAR(100) NOT NULL ,`盗難防止装置の有無` VARCHAR(100) NOT NULL ,`現金以外の主な被害品` VARCHAR(100) NOT NULL , `経度` DOUBLE NOT NULL , `緯度` DOUBLE NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE utf8mb4_general_ci;",
        "CREATE TABLE IF NOT EXISTS `testmap`.`zitensyatou` ( `id` INT NOT NULL AUTO_INCREMENT , `罪名` VARCHAR(100) NOT NULL , `手口` VARCHAR(100) NOT NULL , `管轄警察署（発生地）` VARCHAR(100) NOT NULL , `管轄交番・駐在所（発生地）` VARCHAR(100) NOT NULL , `市区町村コード（発生地）` INT NOT NULL , `都道府県（発生地）` VARCHAR(100) NOT NULL , `市区町村（発生地）` VARCHAR(100) NOT NULL , `町丁目（発生地）` VARCHAR(100) NOT NULL , `発生年月日（始期）` VARCHAR(20) NOT NULL , `発生時（始期）` VARCHAR(20) NOT NULL , `発生場所の属性` VARCHAR(100) NOT NULL , `被害者の年齢` VARCHAR(100) NOT NULL ,`被害者の職業` VARCHAR(100) NOT NULL ,`施錠関係` VARCHAR(100) NOT NULL , `経度` DOUBLE NOT NULL , `緯度` DOUBLE NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB CHARSET=utf8mb4 COLLATE utf8mb4_general_ci;",
    }
    cursor = connection.cursor()
    for row in sql:
        cursor.execute(row)
    # クエリの実行
    connection.commit()
    cursor.close()
    connection.close()
    time.sleep(10)
