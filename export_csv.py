# coding:utf-8
import os
import csv
import re
import datetime
import xlrd
import subprocess


def export_csv(filename, extension):
    print("[Export]")
    # エクスポート元ディレクトリの指定
    target_dir = "./originFiles/"+extension
    # エクスポート元ディレクトリの作成
    os.makedirs(target_dir, exist_ok=True)
    # エクスポート元のパスを生成
    target_path = os.path.join(target_dir, filename+"."+extension)
    # エクスポート先ディレクトリの指定
    dest_dir = './exportedFiles'
    # エクスポート先ディレクトリの作成
    os.makedirs(dest_dir, exist_ok=True)
    # エクスポートの実行
    print(target_path+" is exporting.")
    # 拡張子がcsvだったらdest_dirにコピーして終了
    if extension == 'csv':
        args = ['cp', target_path, dest_dir]
        try:
            subprocess.run(args)
        except:
            print("Error.")
        return print('\033[32m'+"Exported!"+'\033[0m')
    # TODO:csv,xlsx,xls以外のファイルが来た場合の例外処理

    # ここから
    # ブックを読み込みます。
    book = xlrd.open_workbook(target_path)
    # シートでループ
    for sheet in book.sheets():
        dest_path = os.path.join(dest_dir, filename + '.csv')
        with open(dest_path, 'w', encoding='utf-8') as fp:
            writer = csv.writer(fp)
            for row in range(sheet.nrows):
                li = []
                for col in range(sheet.ncols):
                    cell = sheet.cell(row, col)
                    if cell.ctype == xlrd.XL_CELL_NUMBER:  # 数値
                        val = cell.value
                        if val.is_integer():
                            # 整数に'.0'が付与されていたのでintにcast
                            val = int(val)
                    elif cell.ctype == xlrd.XL_CELL_DATE:  # 日付
                        dt = get_dt_from_serial(cell.value)
                        val = dt.strftime('%Y-%m-%d %H:%M:%S')
                    else:  # その他
                        val = cell.value
                    li.append(val)
                writer.writerow(li)
    # ここまで
    print('\033[32m'+"Exported!"+'\033[0m')

# ここから
def get_dt_from_serial(serial):
    """日付のシリアル値をdatetime型に変換します。"""
    base_date = datetime.datetime(1899, 12, 30)
    d, t = re.search(r'(\d+)(\.\d+)', str(serial)).groups()
    return base_date + datetime.timedelta(days=int(d)) \
        + datetime.timedelta(seconds=float(t) * 86400)
# ここまで


if __name__ == '__main__':
    print("Filename?：", end="")
    filename = input()
    print("Extension?：", end="")
    extension = input()
    export_csv(filename, extension)

# 参考：https://remotestance.com/blog/3238/